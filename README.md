make_avr_tags
=============

Simple python script to create a local gtags_external dir and populate it with the correct header files
depending on MCU type. It then create TAG files on the project directory, including the gtags_external
directory.

Can use as a git submodule in any avr-gcc project.

Issue the following from your top level project dir.

``` text
git submodule add  https://bitbucket.org/pymaximus/make_avr_tags.git
```

Note: always run it from your top project directory, not from this sub-module dir.

You can use the tool as follows:

``` text
~/repos/attiny85_fast_pwm [master|✔] 
16:06 $ ./make_avr_tags/make_tags.py -mmcu attiny85 ./main.c 

[Sat Sep 14 16:06:11 AEST 2019] Gtags started.
 Using configuration file '/etc/gtags.conf'.
 Using configuration label 'default'.
[Sat Sep 14 16:06:11 AEST 2019] Creating 'GTAGS' and 'GRTAGS'.
 [1] extracting tags of gtags_external/usr/lib/gcc/avr/9.2.0/include/stdbool.h
 [2] extracting tags of gtags_external/usr/lib/gcc/avr/9.2.0/include/stdint.h
 [3] extracting tags of gtags_external/usr/avr/include/util/delay.h
 [4] extracting tags of gtags_external/usr/avr/include/util/delay_basic.h
 [5] extracting tags of gtags_external/usr/avr/include/math.h
 [6] extracting tags of gtags_external/usr/avr/include/inttypes.h
 [7] extracting tags of gtags_external/usr/avr/include/stdint.h
 [8] extracting tags of gtags_external/usr/avr/include/avr/io.h
 [9] extracting tags of gtags_external/usr/avr/include/avr/portpins.h
 [10] extracting tags of gtags_external/usr/avr/include/avr/iotn85.h
 [11] extracting tags of gtags_external/usr/avr/include/avr/version.h
 [12] extracting tags of gtags_external/usr/avr/include/avr/lock.h
 [13] extracting tags of gtags_external/usr/avr/include/avr/common.h
 [14] extracting tags of gtags_external/usr/avr/include/avr/fuse.h
 [15] extracting tags of gtags_external/usr/avr/include/avr/sfr_defs.h
 [16] extracting tags of gtags_external/usr/avr/include/avr/interrupt.h
 [17] extracting tags of gtags_external/usr/avr/include/avr/iotnx5.h
 [18] extracting tags of main.c

```

The top level project dir should have have 3 new files and 2 new directories.
Note the presence of **GPATH**, **GRTAGS**, **GTAGS** files as well as **gtags_external** directory
and **make_avr_tags** submodule directory.

```
/repos/attiny85_fast_pwm [master|✚ 1] 
16:09 $ ll
total 240
-rw-r--r--. 1 frank frank 16384 Sep 14 16:06 GPATH           <-- new
-rw-r--r--. 1 frank frank 57344 Sep 14 16:06 GRTAGS          <-- new
-rw-r--r--. 1 frank frank 81920 Sep 14 16:06 GTAGS           <-- new
drwxrwxr-x. 3 frank frank  4096 Sep 14 16:06 gtags_external  <-- new
drwxrwxr-x. 2 frank frank  4096 May 22 13:47 images
-rwxrwxr-x. 1 frank frank    90 Sep 14 14:08 main.bin
-rw-rw-r--. 1 frank frank  1450 Sep 13 23:12 main.c
-rwxrwxr-x. 1 frank frank  9768 Sep 14 14:08 main.elf
-rw-rw-r--. 1 frank frank   271 Sep 14 14:08 main.hex
-rw-rw-r--. 1 frank frank  4187 Sep 14 14:08 main.lst
-rw-rw-r--. 1 frank frank 12011 Sep 14 14:08 main.map
-rw-rw-r--. 1 frank frank  5768 Sep 14 14:08 main.o
-rwxrwxr-x. 1 frank frank   294 Sep 14 14:08 main.srec
drwxrwxr-x. 2 frank frank  4096 Sep 14 16:12 make_avr_tags   <-- new submodule
-rw-rw-r--. 1 frank frank  2220 Sep 12 23:35 Makefile
-rwxrwxr-x. 1 frank frank   201 Sep 12 23:39 on_fedora.sh
-rwxrwxr-x. 1 frank frank   187 Sep 12 23:35 on_mac.sh
-rw-rw-r--. 1 frank frank  5711 May 22 13:47 README.md

```



This will help with navigation and searching tags from within Emacs.

