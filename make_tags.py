#!/usr/bin/env python3
# coding: utf-8
#
# PROTOTYPE:
#
# make_tags - Create tags based on avr-gcc preprocessor include paths and global gtags executable.
#
# Copyright (C) 2019 - Frank Singleton
#
# Usage:
#
#   ./make_tags.py -mmcu=attiny85 ./main.c
#
# Creates GTAGS, GRTAGS, GPATH files and gtags_external directory.
#
# This is so I can use ggtags from Emacs.

# On Fedora
#
# 00:13 $ avr-cpp -D__AVR_ATtiny85__ -M ./main.c
# main.o: main.c /usr/avr/include/inttypes.h \
#  /usr/lib/gcc/avr/9.2.0/include/stdint.h /usr/avr/include/stdint.h \
#  /usr/lib/gcc/avr/9.2.0/include/stdbool.h /usr/avr/include/avr/io.h \
#  /usr/avr/include/avr/sfr_defs.h /usr/avr/include/avr/iotn85.h \
#  /usr/avr/include/avr/iotnx5.h /usr/avr/include/avr/portpins.h \
#  /usr/avr/include/avr/common.h /usr/avr/include/avr/version.h \
#  /usr/avr/include/avr/fuse.h /usr/avr/include/avr/lock.h \
#  /usr/avr/include/util/delay.h /usr/avr/include/util/delay_basic.h \
#  /usr/avr/include/math.h /usr/avr/include/avr/interrupt.h

#
# Another projct on Mac
#
# ✔ ~/repos/atmega32_conway_gol [uno_r3_conway_gol|✔]
# 00:18 $  avr-cpp -D__AVR_ATtiny85__ -M ./main.c
# main.o: main.c defines.h i2cmaster.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/io.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/sfr_defs.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/inttypes.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/lib/avr-gcc/9/gcc/avr/9.1.0/include/stdint.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/stdint.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/iotn85.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/iotnx5.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/portpins.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/common.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/version.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/fuse.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/lock.h myFont.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/pgmspace.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/lib/avr-gcc/9/gcc/avr/9.1.0/include/stddef.h \
#  myBitMaps.h ssd1306.h frameBuffer.h world.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/string.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/stdlib.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/util/delay.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/util/delay_basic.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/math.h \
#  /usr/local/Cellar/avr-gcc/9.1.0/avr/include/avr/interrupt.h

#
# So, it seems we can skip the first 2 entries, eg: main.o: and main.c
# TODO: verify this
#


#
# standard imports
#
import argparse
import os
import os.path
import platform
import shutil
import subprocess
import sys

# 3rd party modules

# local modules



# find gtags and avr-cpp depending on platform
if platform.system() == 'Linux':
    # eg: Fedora
    _GTAGS_EXTERNAL_DIR = './gtags_external'
    _GTAGS_PATH = '/usr/bin/gtags'
    _AVR_CPP = '/usr/bin/avr-cpp'
elif platform.system() == 'Darwin':
    # eg: Macbook pro
    _GTAGS_EXTERNAL_DIR = './gtags_external'
    _GTAGS_PATH = '/usr/local/bin/gtags'
    _AVR_CPP = '/usr/local/bin/avr-cpp'


# Map mmcu to CPP Macro
# TODO: add other symbols
_MMCU_CPP_SYMBOL = {
    'attiny85':	'__AVR_ATtiny85__',
    'attiny861': '__AVR_ATtiny861__',
    'attiny861a': '__AVR_ATtiny861A__',
    'atmega328p': '__AVR_ATmega328P__'
}


def do_gtags():
    """Execute gtags -v from current directory."""
    try:
        if os.path.exists('GTAGS'):
            os.remove('GTAGS')
        if os.path.exists('GRTAGS'):
            os.remove('GRTAGS')
        if os.path.exists('GPATH'):
            os.remove('GPATH')
    except:
        print('Error removing files GTAGS/GRTAGS/GPATH')
        sys.exit(1)

    # run gtags
    output = subprocess.check_output([_GTAGS_PATH, '-v'])


def get_include_paths(filename, mmcu, verbose=False):
    """Run avr-cpp and fetch include paths as tuple."""
    cpp_symbol = _MMCU_CPP_SYMBOL[mmcu]

    output = subprocess.check_output([_AVR_CPP, '-D' + cpp_symbol, '-M', filename], encoding='UTF-8')
    # remove \ and newlines, then split on whitespace
    lines = output.replace('\\', ' ').replace('\n',' ').split()
    #print(lines)
    # cleanup output
    clean_paths = []
    for entry in lines[2:]:     # skip first 2 entries, main.o: main.c
        entry = entry.strip()
        if verbose:
            print(entry)
        clean_paths.append(entry)

    if verbose:
        print(clean_paths)

    #print(lines)
    return tuple(clean_paths)


def do_main():
    # handle args
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('-v', action='store_true', help='Verbose output mainly for trouble-shooting')


    parser.add_argument('filename', action='store', help='Filename (eg: main.c) to analyze')

    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-mmcu', action='store', help='MCU Target (eg: attiny85)', required=True,
                                choices=_MMCU_CPP_SYMBOL.keys())

    args  = parser.parse_args()

    # get header paths for avr-cpp preprocessor
    include_paths = get_include_paths(args.filename, args.mmcu, args.v)

    # remove and re-create gtags_external dir
    shutil.rmtree(_GTAGS_EXTERNAL_DIR, True)
    os.mkdir(_GTAGS_EXTERNAL_DIR)

    # copy include files to gtags_external dir, preserving source path structure
    for entry in include_paths:
        if args.v:
            print(entry)

        dest = os.path.join(_GTAGS_EXTERNAL_DIR, entry[1:])
        if args.v:
            print(dest)

        dest_folder = os.path.dirname(dest)

        if not os.path.exists(dest_folder):
            os.makedirs(dest_folder)
        # copy entry to destination
        shutil.copy(entry, dest)


    # now finallly run gtags on populated gtags_external dir
    do_gtags()

if __name__ == '__main__':
    try:
        do_main()

    except:
        sys.stderr.write('Error executing program.')
        sys.exit(1)
